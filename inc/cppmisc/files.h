#pragma once
#include <sys/stat.h>
#include <string>
#include <vector>

void writefile(std::string const& path, void const* data, size_t size);
void writefile(std::string const& path, std::string const& data);
bool file_exists(char const* filepath);
bool file_exists(std::string const& filepath);
int64_t file_size(char const* filepath);
bool dir_exists(char const* dirpath);
bool dir_exists(std::string const& dirpath);
std::string read_all(std::string const& file);
std::tuple<std::string, std::string> splitname(std::string const& path);
std::tuple<std::string, std::string> splitext(std::string const& path);
std::string getname(std::string const& path);
std::string getext(std::string const& path);
std::vector<std::string> get_files(std::string const& mask);
