#include <cppmisc/signals.h>
#include <cppmisc/traces.h>
#include <signal.h>
#include <assert.h>


void SysSignals::set_sigint_handler(sighandler_t handler)
{
    _sigint_handler = std::move(handler);
    signal(SIGINT, &SysSignals::handle);
}

void SysSignals::set_sigterm_handler(sighandler_t handler)
{
    _sigterm_handler = std::move(handler);
    signal(SIGTERM, &SysSignals::handle);
}

void SysSignals::set_sighup_handler(sighandler_t handler)
{
    assert(false);
}

void SysSignals::handle(int sig)
{
    SysSignals& inst = SysSignals::instance();

    switch (sig)
    {
    case SIGTERM:
        if (inst._sigterm_handler)
            inst._sigterm_handler();
        break;
    case SIGINT:
        if (inst._sigint_handler)
            inst._sigint_handler();
        break;
    }
}

SysSignals& SysSignals::instance()
{
    static SysSignals signals;
    return signals;
}

SysSignals::SysSignals()
{
}

SysSignals::~SysSignals()
{
}
