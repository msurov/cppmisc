#pragma once

#include <json/json.h>
#include "throws.h"


namespace Json {
struct ConvertationError : Json::Exception {
    ConvertationError(Json::String const& msg) : Json::Exception(msg) {}
};

}

template <class T>
struct Converter {
    static T doit(Json::Value const& json);
};

template <>
struct Converter<Json::Value> {
    static Json::Value doit(Json::Value const& json) { return json; }
};

template <>
struct Converter<double> {
    static double doit(Json::Value const& json) { return json.asDouble(); }
};

template <>
struct Converter<float> {
    static float doit(Json::Value const& json) { return json.asFloat(); }
};

template <>
struct Converter<std::string> {
    static std::string doit(Json::Value const& json) { return json.asString(); }
};

template <>
struct Converter<int> {
    static int doit(Json::Value const& json) { return json.asInt(); }
};

template <>
struct Converter<int64_t> {
    static int64_t doit(Json::Value const& json) { return json.asInt64(); }
};

template <typename T>
struct Converter<std::vector<T>> {
    static std::vector<T> doit(Json::Value const& json)
    {
        const int n = json.size();
        std::vector<T> arr;
        arr.reserve(n);
        for (int i = 0; i < n; ++ i)
            arr.push_back(Converter<T>::doit(json[i]));
        return arr;
    }
};

template <typename T, size_t N>
struct Converter<std::array<T, N>> {
    static std::array<T, N> doit(Json::Value const& json)
    {
        const int n = json.size();
        if (n != N)
            throw_exception<Json::ConvertationError>("Can't parse std::array, incorrect size: expect ", N, " but actual is ", n);
        std::array<T, N> arr;
        for (int i = 0; i < n; ++ i)
            arr[i] = Converter<T>::doit(json[i]);
        return arr;
    }
};

template <typename T=Json::Value>
inline T json_get(Json::Value const& json, char const* name)
{
    if (!json.isMember(name))
        throw_exception<Json::LogicError>("Json ", json, " does not contain entry ", name);
    
    if constexpr (std::is_same_v<T, Json::Value>)
    {
        return json[name];
    }
    else
    {
        try
        {
            return Converter<T>::doit(json[name]);
        }
        catch (Json::ConvertationError const& e)
        {
            throw_exception<Json::ConvertationError>("Failed parsing field \"", name, "\": ", e.what());
        }
    }
}

template <typename T>
inline void json_get(Json::Value const& json, char const* name, T& value)
{
    if (!json.isMember(name))
        throw_exception<Json::LogicError>("Json ", json, " does not contain entry \"", name, "\"");

    try
    {
        value = Converter<T>::doit(json[name]);
    }
    catch (Json::ConvertationError const& e)
    {
        throw_exception<Json::ConvertationError>("Failed parsing field \"", name, "\": ", e.what());
    }
}

template <typename T>
inline void json_get(Json::Value const& json, int idx, T& value)
{
    try
    {
        value = Converter<T>::doit(json[idx]);
    }
    catch (Json::ConvertationError const& e)
    {
        throw_exception<Json::ConvertationError>("Failed parsing ", idx, "-th element: ", e.what());
    }
}

template <typename T>
inline T json_get(Json::Value const& json, int idx)
{
    try
    {
        return Converter<T>::doit(json[idx]);
    }
    catch (Json::ConvertationError const& e)
    {
        throw_exception<Json::ConvertationError>("Failed parsing ", idx, "-th element: ", e.what());
    }
}

Json::Value json_load(std::string const& filepath);
Json::Value json_parse(char const* str);
inline Json::Value json_parse(std::string const& s) { return json_parse(s.c_str()); }

inline bool json_has(Json::Value const& v, std::string const& name)
{
    if (!v.isMember(name))
        return false;
    return true;
}
