#include <cppmisc/files.h>
#include <cppmisc/throws.h>
#include <cppmisc/strings.h>

#include <filesystem>
#include <sys/types.h>
#include <sys/stat.h>

#include <tuple>
#include <string>
#include <fstream>


using namespace std;

tuple<string, string> splitname(string const& path)
{
    auto n = path.find_last_of("/\\");
    if (n == string::npos)
        return make_tuple("", path);
    n += 1;
    return make_tuple(path.substr(0, n), path.substr(n));
}

tuple<string, string> splitext(string const& path)
{
    auto n = path.find_last_of("/\\.");
    if (n == string::npos)
        return make_tuple(path, "");

    if (path[n] == '/' || path[n] == '\\')
        return make_tuple(path, "");

    return make_tuple(path.substr(0, n), path.substr(n));
}

string getname(string const& path)
{
    auto n = path.find_last_of("/\\");
    if (n == string::npos)
        return "";
    return path.substr(n + 1);
}

string getext(string const& path)
{
    auto n = path.find_last_of("/\\.");
    if (n == string::npos)
        return "";

    if (path[n] == '/' || path[n] == '\\')
        return "";

    return path.substr(n);
}

string read_all(string const& file)
{
    ifstream f(file);
    if (!f.good())
        throw_runtime_error("can't open ", file);

    f.seekg(0, f.end);
    int64_t length = f.tellg();
    f.seekg(0, f.beg);

    if (length < 0)
        throw_runtime_error("failed requiring file size");

    string data;
    data.resize(length);
    f.read(&data[0], data.size());
    if (!f.good())
        throw_runtime_error("failed to read file ", file);

    return data;
}

std::vector<std::string> get_files(std::string const& mask)
{
    vector<string> files;

    string dirpath;
    string namemask;
    tie(dirpath, namemask) = splitname(mask);

    std::filesystem::directory_iterator dirit{dirpath};

    for (auto const& f : dirit)
    {
        if (!f.is_regular_file())
            continue;

        auto const& path = f.path();
        auto const& name = path.filename();

        if (!match_mask(name.string(), namemask))
            continue;

        files.push_back(path.string());
    }

    return files;
}

bool file_exists(char const* filepath)
{
    return std::filesystem::is_regular_file(filepath);
}

bool file_exists(std::string const& filepath)
{
    return std::filesystem::is_regular_file(filepath);
}

int64_t file_size(char const* filepath)
{
    return std::filesystem::file_size(filepath);
}

bool dir_exists(char const* dirpath)
{
    return std::filesystem::is_directory(dirpath);
}

bool dir_exists(std::string const& dirpath)
{
    return std::filesystem::is_directory(dirpath);
}
